import os
import sys
import shutil
import tempfile
import re
import requests

def cls():
	# A lazy way to implement Windows/Linux screen clearing.
	os.system(['clear','cls'][os.name == 'nt'])

def prependToFile(f, text):
	# A simple function to make it easier to prepend text to file.
	outFile = tempfile.NamedTemporaryFile(dir='.', delete=False)
	outFile.write('%s\n' % str(text))
	shutil.copyfileobj(file(f, 'r'), outFile)
	os.remove(f)
	shutil.move(outFile.name, f)
	outFile.close()

def prependConfig():
	# Add the NameSpace polluting import statement. Without it, the config file doesn't work.
	prependToFile("src/Config.py", "from Proxy import *")

def localReferenceReplace():
	# Replace the local references with variables from src/Proxy.py
	with open("src/Config.py", "r") as sources:
		lines = sources.readlines()
	with open("src/Config.py", "w") as sources:
		for line in lines:
			sources.write(re.sub(r'"127.0.0.1"', 'proxyAddress', line))

def choose_install():
	# A menu to add or remove the proxy settings.
	print "Do you wish to INSTALL or UNINSTALL Proxify?"
	print ""
	print "* INSTALL"
	print "* UNINSTALL"
	print ""
	install = (['install','instal','get'])
	uninstall = (['uninstall','uninstal','rm'])
	cancel = (['cancel', 'quit', 'kill'])
	choice = raw_input().lower()
	while choice not in ['install','instal','get','uninstall','uninstal','rm','cancel','quit','kill']:
		choice = raw_input().lower()
	if choice in install:
		warning()
	if choice in uninstall:
		deProxify()
	if choice in cancel:
		sys.exit("Bye!")
	else:
		sys.stdout.write("Please respond with 'install', 'uninstall' or 'cancel'")

def warning():
	# Warn people that Proxifying isn't entirely intelligent.
	# It requires you think through what you are wanting to do.
	cls()
	print "Proxifying ZeroNet intentionally:"
	print ""
	print "1. Exposes your server to the internet."
	print "2. Pollutes the zeronet.py NameSpace."
	print "3. This only works with Python 2.x, NOT Python 3.x"
	print ""
	print "This is not a great idea."
	print ""
	print "Answer 'yes' to continue, or 'no' to exit."
	yes = set(['yes','y', 'ye', ''])
	no = set(['no','n'])
	choice = raw_input().lower()
	while choice not in ['yes','y', 'ye','no','n']:
		choice = raw_input().lower()
	if choice in yes:
		proxify()
	elif choice in no:
		sys.exit("You decided not to Proxify your ZeroNet Installation.")
	else:
		sys.stdout.write("Please respond with 'yes' or 'no'")

def genProxy():
	# Check if src/Proxy.py exists.
	if os.path.isfile("src/Proxy.py"):
		print "Proxy.py exists, skipping."
	else:
		# Download Proxy.py from the repo, rather than creating it locally.
		print "Proxy.py not found, downloading it."
		url = "https://raw.githubusercontent.com/zeronetproxy/Config.py/master/Config.py"
		response = requests.get(url, stream=True)
		with open('src/Proxy.py', 'wb') as out_file:
    			for chunk in response.iter_content(1024):
				out_file.write(chunk)
		del response

		print "Proxy.py downloaded."

def proxify():
	# The main install function.
	cls()
	print "Backing up src/Config.py"
	shutil.copyfile("src/Config.py","src/Config.py.bak")
	print "Config file backed up."
	print "Adding reference to Proxy config file to main ZeroNet config file."
	prependConfig()
	print "Reference added to config file."
	print "Replacing all local references with Proxy config file references."
	localReferenceReplace()
	print "Replace local references."
	print "Checking if src/Proxy.py exists..."
	genProxy()
	print "ZeroNet Installation is Proxified!"

def repairConfig():
	proxyConfig = 'src/Config.py'
	if os.path.isfile(proxyConfig):
		# Remove the import statement.
		file = open(proxyConfig)
		output = []
		for line in file:
			if not "from Proxy import *" in line:
				output.append(line)
		file.close()
		file = open(proxyConfig, 'w')
		file.writelines(output)
		file.close()
	else:
		print "src/Config.py not found!"
		print "THIS IS PROBABLY AN ERROR"
	# Remove variable references from the config file.
	with open("src/Config.py", "r") as sources:
		lines = sources.readlines()
	with open("src/Config.py", "w") as sources:
		for line in lines:
			sources.write(re.sub(r'proxyAddress', '"127.0.0.1"', line))

def rmProxyConfig():
	# Remove the src/Proxy.py file.
	proxyConfig = 'src/Proxy.py'
	if os.path.isfile(proxyConfig):
		os.remove(proxyConfig)
		print "src/Proxy.py removed."
	else:
		print "src/Proxy.py not found!"
		print "THIS IS PROBABLY AN ERROR!"

def deProxify():
	# The main uninstall function
	cls()
	print "Starting deProxify..."
	print "Removing Proxy references in Config file..."
	repairConfig()
	print "Replaced all local references in Config file."
	print "Checking for src/Proxy.py..."
	rmProxyConfig()
	print "Removed Proxy config file."
	print "ZeroNet Installation is deProxified!"
	

def main():
	choose_install()

main()
